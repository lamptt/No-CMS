<?php
class Orders_Model extends CMS_Model{

    /**
     * @author applelam
     * @param
     * @return void
     * @desc deposit
     **/
    function create($link, $quantity, $price, $hour, $type = 'normal'){
        $data = array(
            'views_user_id'=> $this->cms_user_id(),
            'views_link' => $link,
            'views_quantity' => $quantity,
            'views_price' => $price,
            'views_frame_hours' => $hour,
            'views_type' => $type,
            //'views_state' => 'closed',
        );
        $this->db->insert('views', $data);
    }

    function getInformationUser(){
        $where = array(
            'user_id'=> $this->cms_user_id()
        );
        $this->db->select('la_main_user_credits, la_main_user_type');
        $query = $this->db->get_where($this->cms_complete_table_name('main_user', ''), $where, 1);
        if($query->num_rows()>0){
            $row = $query->row();
            return $row;
        }
    }

    function getId($link)
    {
        $findme   = '&feature';
        $pos = strpos($link, $findme);
        if ($pos === false) {
            return substr($link, 32);
        } else {
            return substr($link, 32, 11);
        }

    }

}