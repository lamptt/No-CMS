<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Manage_Category
 *
 * @author No-CMS Module Generator
 */
class Process_orders extends CMS_Secure_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->cms_module_path().'/process_model');
    }

	public function index(){
        $crud = $this->new_crud();
        $crud->unset_jquery();

        // adjust groceryCRUD's language to No-CMS's language
        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table('views');

        // set subject
        $crud->set_subject('Orders');

        // displayed columns on list
        $crud->columns('views_id', 'views_state', 'views_frame_hours', 'views_type', 'views_quantity', 'views_link');
        // displayed columns on edit operation
        $crud->edit_fields('views_state', 'views_link','views_quantity','views_price');
        // displayed columns on add operation
        $crud->add_fields('views_state', 'views_link','views_quantity','views_price');
        // required field
        $crud->required_fields('views_id');
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_edit();
        $crud->unset_delete();

        // caption of each columns
        $crud->display_as('views_id','Id');
        $crud->display_as('views_state','Tình trạng');
        $crud->display_as('views_frame_hours','Khung giờ(UTC +8)');
        $crud->display_as('views_type','Loại view');
        $crud->display_as('views_quantity','Số lượng');
        $crud->display_as('views_link','Link');

        $crud->field_type('views_state', 'enum', array('open','pending','closed'));

        $crud->where('views_state', 'open')
            ->or_where('views_state', 'error')
            ->order_by('views_state, views_type, views_quantity');

        $crud->add_action('Close', '', '','btn btn-default',array($this,'just_a_test'));

        $output = $crud->render();

        // prepare css & js, add them to config
        $config = array();
        $asset = new Cms_asset();
        foreach($output->css_files as $file){
            $asset->add_css($file);
        }
        $config['css'] = $asset->compile_css();

        foreach($output->js_files as $file){
            $asset->add_js($file);
        }
        $config['js'] = $asset->compile_js();

        // show the view
        $this->view('youtube/process_orders_view', $output,
            'youtube_process_orders', $config);

    }

    function just_a_test($primary_key , $row)
    {
        //if ($row->views_state == 'open') return site_url('youtube/process_orders/close').'/'.$row->views_frame_hours;
        return site_url('youtube/process_orders/closeid').'/'.$row->views_id;
    }

    public function close($frame){
        $currentDate = new DateTime();
        $hour = intval($currentDate->format('H'));
        if ($frame<$hour || $frame == '23'){
            $this->process_model->close($frame);
            redirect('/youtube/process_orders');
        }
        else {
            $data = array(
                "error" => $this->cms_lang('error-hour'),
            );
            $this->view('youtube/process_orders_error_view', $data);
        }
    }
    public function closeid($id){
        $this->process_model->closeid($id);
        redirect('/youtube/process_orders');
    }

    public function before_insert($post_array){
		return TRUE;
	}

	public function after_insert($post_array, $primary_key){
		$success = $this->after_insert_or_update($post_array, $primary_key);
		return $success;
	}

	public function before_update($post_array, $primary_key){
		return TRUE;
	}

	public function after_update($post_array, $primary_key){
		$success = $this->after_insert_or_update($post_array, $primary_key);
		return $success;
	}

	public function before_delete($primary_key){

		return TRUE;
	}

	public function after_delete($primary_key){
		return TRUE;
	}

	public function after_insert_or_update($post_array, $primary_key){

        return TRUE;
	}



}