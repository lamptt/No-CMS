<?php
class Credits_Model extends CMS_Model{

    /**
     * @author applelam
     * @param float credits - amount of deposit
     * @param int views - TRUE:deposit FALSE:buy view
     * @return void
     * @desc deposit
     **/
    function deposit($credits = 0, $view = 0){
        //1. get $user_credits
        $where = array(
            'user_id'=> $this->cms_user_id()
        );
        $this->db->select('la_main_user_credits, la_main_user_money, la_main_user_sumviews');
        $query = $this->db->get_where($this->cms_complete_table_name('main_user', ''), $where, 1);
        $user_credits = 0;
        $user_money = 0;
        $sumviews = 0;
        if($query->num_rows()>0){
            $row = $query->row();
            $user_credits = floatval($row->la_main_user_credits);
            $user_money = floatval($row->la_main_user_money);
            $sumviews = intval($row->la_main_user_sumviews);
        }

        //$data = array();
        //2. edit $credits
        $user_credits = round($user_credits + $credits, 2);
        if ($view){
            if ($user_credits < 0) exit;
            $sumviews += $view;
            $data = array(
                'la_main_user_credits' => $user_credits,
                'la_main_user_sumviews' => $sumviews,
            );
        }
        else{
            $user_money += $credits;
            $data = array(
                'la_main_user_credits' => $user_credits,
                'la_main_user_money' => $user_money,
            );
        }
        $this->db->update($this->cms_complete_table_name('main_user', ''), $data, $where);
        $this->cms_user_credits($user_credits);
        if ($credits < 0){
            $poin = (-$credits) + $this->cms_user_poin();
            $this->cms_user_poin($poin);
        }
    }

}