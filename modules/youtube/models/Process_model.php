<?php
class Process_Model extends CMS_Model{

    /**
     * @author applelam
     * @param int $frame
     * @return void
     * @desc close
     **/
    function close($frame){
        //1. get $user_credits
        $where = array(
            'views_state' => 'open',
            'views_frame_hours' => $frame
        );
        $data = array(
            'views_state' => 'closed',
        );
        $this->db->update('views', $data, $where);
    }

    /**
     * @author applelam
     * @param int id
     * @return void
     * @desc deposit
     **/
    function closeid($id){
        //1. get $user_credits
        $where = array(
            'views_id' => $id
        );
        $data = array(
            'views_state' => 'closed',
        );
        $this->db->update('views', $data, $where);
    }

}