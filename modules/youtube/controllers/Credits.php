<?php
class Credits extends CMS_Secure_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->cms_module_path().'/credits_model');
        $this->load->model($this->cms_module_path().'/paypal_model');
        $this->load->library('youtube/paypal');
    }

    private function __random_string($length=10)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < $length; $i++ ){
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str.='_';
    }
    /**
     * @author applelam
     * @return void
     * @desc deposit(gui tien)
     **/
    function index(){
        // the honey_pot, every fake input should be empty
        $honey_pot_pass = (strlen($this->input->post('credits', ''))==0);
        if(!$honey_pot_pass){
            show_404();
            die();
        }

        $previous_secret_code = $this->session->userdata('__credits_secret_code');
        if($previous_secret_code === NULL){
            $previous_secret_code = $this->__random_string();
        }

        //get input
        $credits = $this->input->post($previous_secret_code.'credits');

        //set validation rule
        $this->form_validation->set_rules($previous_secret_code.'credits', 'Amount', 'required|min_length[2]|is_natural_no_zero');

        // generate new secret code
        $secret_code = $this->__random_string();
        $this->session->set_userdata('__credits_secret_code', $secret_code);

        if ($this->form_validation->run())
        {
            $approvalUrl = $this->paypal->payments($credits);
            redirect($approvalUrl);

        }
        else
        {
            $data = array(
                "credits" => $credits,
                "register_caption" => $this->cms_lang('Paypal'),
                "secret_code" => $secret_code,
            );
            $this->view($this->cms_module_path().'/credits_index', $data);
        }
    }

    /**
     * @author applelam
     * @return void
     * @desc executepayment deposit
     **/
    public function executepayment(){
        $payment = $this->paypal->executepayment();
        $total = $payment->transactions[0]->amount->total;
        $payment_id = $payment->id;
        //1. Save db
        $this->credits_model->deposit($total);
        $this->paypal_model->create($payment_id);
        //2. View
        $data = array(
            "total" => $total,
            "currency" => $payment->transactions[0]->amount->currency,
            "email" => $payment->payer->payer_info->email,
        );
        $this->view($this->cms_module_path().'/credits_success', $data);
    }

    /**
     * @author applelam
     * @return void
     * @desc check deposit
     **/
    public function check_deposit()
    {
        if ($this->input->is_ajax_request()) {
            $credits = $this->input->post('credits');

            $message   = "";
            $error = FALSE;
            if (!is_numeric($credits)) {
                $message = $this->cms_lang("is NOT numeric");
                $error = TRUE;
            }
            else if ($credits<10){
                $message = $this->cms_lang("is less 10");
                $error = TRUE;
            }

            $data = array(
                "error" => $error,
                "message" => $message
            );
            $this->cms_show_json($data);
        }
    }
}