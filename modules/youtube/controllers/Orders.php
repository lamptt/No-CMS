<?php
class Orders extends CMS_Secure_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->cms_module_path().'/credits_model');
        $this->load->model($this->cms_module_path().'/orders_model');
        $this->load->config('youtube/orders');
        //like return
        //$this->config->load('youtube/orders');
    }

    private function getInformationUser(){
        return $this->orders_model->getInformationUser();
    }

    private function __random_string($length=10)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < $length; $i++ ){
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str.='_';
    }
    /**
     * @author applelam
     * @return void
     * @desc orders
     **/
    function index(){
        // the honey_pot, every fake input should be empty
        $honey_pot_pass = (strlen($this->input->post('link', ''))==0
            && (strlen($this->input->post('quantity', ''))==0)
            && (strlen($this->input->post('type', ''))==0)
        );
        if(!$honey_pot_pass){
            show_404();
            die();
        }

        $previous_secret_code = $this->session->userdata('__credits_secret_code');
        if($previous_secret_code === NULL){
            $previous_secret_code = $this->__random_string();
        }

        //get input
        $link = $this->input->post($previous_secret_code.'link');
        $quantity = intval($this->input->post($previous_secret_code.'quantity'));
        $type = $this->input->post($previous_secret_code.'type');

        //set validation rule
        $this->form_validation->set_rules($previous_secret_code.'link', 'Link', 'required|min_length[30]|max_length[60]');
        $this->form_validation->set_rules($previous_secret_code.'quantity', 'Quantity', 'required|is_natural_no_zero');

        // generate new secret code
        $secret_code = $this->__random_string();
        $this->session->set_userdata('__credits_secret_code', $secret_code);

        // get price
        $price_high = $this->config->item('price_high');
        $price = $this->config->item('price');
        if ($this->getInformationUser()->la_main_user_type == 'vip'){
            $price_high -= 0.2;
            $price -= 0.2;
        }

        if ($type) $total = $price_high * $quantity;
        else $total = $price * $quantity;
        if ($this->form_validation->run() && ($total <= $this->getInformationUser()->la_main_user_credits))
        {
            //1. Save database
            $this->credits_model->deposit(-$total, $quantity);
            $currentDate = new DateTime();
            $hour = intval($currentDate->format('H'));
            $typexl = 1;
            if ($type) {
                $this->orders_model->create($link, $quantity, $price_high, $hour, 'high');
                $typexl = 2;
            }
            else $this->orders_model->create($link, $quantity, $price, $hour);
            $vid = $this->orders_model->getId($link);
            $amount = $quantity * 1000;
            $this->load->library('curl');
            // Simple call to CI URI
            $responce = $this->curl->simple_post('http://xlviews.com/api?d=order',
                array(
                    //'key' => 'cmtcg38e71083c1fa8aae5', //key live
                    'key' => 'cmtcg38e71083c1fa8aae6', //key dev
                    'auth' => 'f11000173b8fe4ba2f738c12083e4c8e',
                    'pass' => '28aded121188ed96ea4d4bd53d13c1bc',
                    'type' => $typexl,
                    'vid' => $vid,
                    'amount' => $amount
                )
            );
            //print_r($responce);
            //2. view
            $data = array(
                "link" => $link,
                "total" => $total,
            );
            $this->view($this->cms_module_path().'/orders_success', $data);
        }
        else
        {
            $data = array(
                "link" => $link,
                "quantity" => $quantity,
                "price" => $price,
                "price_high" => $price_high,
                "register_caption" => $this->cms_lang('Ok'),
                "secret_code" => $secret_code,
            );
            $this->view($this->cms_module_path().'/orders_index', $data);
        }
    }

    /**
     * @author applelam
     * @return void
     * @desc check order
     **/
    public function check_order()
    {
        if ($this->input->is_ajax_request()) {
            $link = $this->input->post('link');
            $quantity = $this->input->post('quantity');
            $type = $this->input->post('type');
            $message   = "";
            $error = FALSE;

            $price_high = $this->config->item('price_high');
            $price = $this->config->item('price');
            if ($this->getInformationUser()->la_main_user_type == 'vip'){
                $price_high -= 0.2;
                $price -= 0.2;
            }

            if ($type == 'true') $total = $price_high * $quantity;
            else $total = $price * $quantity;

            if (strlen($link)<30 || strlen($link)>60 || strpos($link, 'youtube.com')==FALSE){
                $message = $this->cms_lang("This is not a link YouTube");
                $error = TRUE;
            }
            if (!is_numeric($quantity)) {
                $message = $this->cms_lang("Quantity is NOT numeric");
                $error = TRUE;
            }
            if ($total > $this->getInformationUser()->la_main_user_credits) {
                $message = $this->cms_lang("You don't enough money");
                $error = TRUE;
            }
            $data = array(
                "error" => $error,
                "message" => $message
            );
            $this->cms_show_json($data);
        }
    }
}