
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
    <style type="text/css">
        #message:empty{
            display:none;
        }
        .order_input, .price, .price_high{
            display:none;
        }
        .price, .price_high{
            padding-top: 7px;
            padding-left: 14px;
        }
    </style>

    <h3>Tìm kiếm video YouTube trong vòng 1 tháng</h3>
<?php
echo form_open_multipart('youtube/search', 'id="form-order" class="form form-horizontal"');
echo form_input(array('name'=>'title', 'value'=>'', 'class'=>'order_input'));
echo form_input(array('name'=>'quantity', 'value'=>'', 'class'=>'order_input'));

echo '<div class="form-group">';
echo form_label('Từ khóa', ' for="" class="control-label col-sm-4');
echo '<div class="col-sm-8">';
echo form_input($secret_code.'title', $link,
    'id="'.$secret_code.'link" placeholder="Từ khóa tìm kiếm" class="form-control"');
echo '</div>';
echo '</div>';

echo '<div class="form-group">';
echo form_label('Số lượng', ' for="" class="control-label col-sm-4');
echo '<div class="col-sm-8">';
echo form_input($secret_code.'quantity', 25,
    'id="'.$secret_code.'quantity" placeholder="Phải là số tự nhiên < 50" class="form-control"');
echo '</div>';
echo '</div>';


echo '<div class="form-group"><div class="col-sm-offset-4 col-sm-8">';
echo '<img id="img_ajax_loader" style="display:none;" src="'.base_url('assets/nocms/images/ajax-loader.gif').'" /><br />';
echo '<div id="message" class="alert alert-danger"></div>';
echo form_submit('btn_order', 'Tìm', 'id="btn-credits" class="btn btn-primary" ');
echo '</div></div>';
echo form_close();

echo $htmlBody;
?>