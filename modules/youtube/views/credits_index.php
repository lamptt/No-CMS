<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
    <style type="text/css">
        #message:empty{
            display:none;
        }
        #btn-credits, .credits_input{
            display:none;
        }
    </style>

    <script type="text/javascript">
        var REQUEST_EXISTS = false;
        var REQUEST = "";
        function check_deposit(){
            var credits =  $('input[name="<?=$secret_code?>credits"]').val();

            $("#img_ajax_loader").show();
            if(REQUEST_EXISTS){
                REQUEST.abort();
            }
            REQUEST_EXISTS = true;
            REQUEST = $.ajax({
                "url" : "credits/check_deposit",
                "type" : "POST",
                "data" : {"credits":credits},
                "dataType" : "json",
                "success" : function(data){
                    if(!data.error && !data.exists && credits!=''){
                        $('input[name="btn_credits"]').show();
                        $('input[name="btn_credits"]').removeAttr('disabled');
                        console.log($('input[name="btn_credits"]'));
                    }else{
                        $('input[name="btn_credits"]').hide();
                        $('input[name="btn_credits"]').attr('disabled', 'disabled');
                    }

                    // get message from server + local check
                    var message = '';
                    if(data.message!=''){
                        message += data.message+'<br />';
                    }

                    if(credits == ''){
                        message += '{{ language:Credits is empty }}<br />';
                    }

                    if(message != $('#message').html()){
                        $('#message').html(message);
                    }
                    REQUEST_EXISTS = false;
                    $("#img_ajax_loader").hide();
                },
                error: function(xhr, textStatus, errorThrown){
                    if(textStatus != 'abort'){
                        setTimeout(check_deposit, 10000);
                    }
                }
            });
        }

        $(document).ready(function(){

            check_deposit();

            $('#form-credits input').keyup(function(){
                check_deposit();
            });

        });
    </script>

    <h3>{{ language:Credits }}</h3>
    <p>{{ language:new-payment-title }}</p>
    <a href="http://valviews.com/blog/index/new-credits" target="_blank">{{ language:new-payment-link }}</a>
    <br/><br/>
    <p>{{ language:paypal-title }}</p>
<?php
echo form_open_multipart('youtube/credits', 'id="form-credits" class="form form-horizontal"');
echo form_input(array('name'=>'credits', 'value'=>'', 'class'=>'credits_input'));

echo '<div class="form-group">';
echo form_label('{{ language:Amount }}', ' for="" class="control-label col-sm-4');
echo '<div class="col-sm-8">';
echo form_input($secret_code.'credits', $credits,
    'id="'.$secret_code.'credits" placeholder="credits" class="form-control"');
echo '</div>';
echo '</div>';


echo '<div class="form-group"><div class="col-sm-offset-4 col-sm-8">';
echo '<img id="img_ajax_loader" style="display:none;" src="'.base_url('assets/nocms/images/ajax-loader.gif').'" /><br />';
echo '<div id="message" class="alert alert-danger"></div>';
echo form_submit('btn_credits', $register_caption, 'id="btn-credits" class="btn btn-primary" ');
echo '</div></div>';
echo form_close();
?>