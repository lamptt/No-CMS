<?php
$lang['language_alias'] = 'en';
$lang['Complete Menu'] = 'Admin';
$lang['Home'] = 'Home';
$lang['Language'] = 'Language';
$lang['Third Party Authentication'] = 'Third Party Authentication';
$lang['Login'] = 'Login';
$lang['Logout'] = 'Logout';
$lang['Forgot Password'] = 'Forgot Password';
$lang['Register'] = 'Register';
$lang['No-CMS User Guide'] = 'User Guide';
$lang['Change Profile'] = 'Profile';
$lang['CMS Management'] = 'CMS Management';
$lang['Group Management'] = 'Groups';
$lang['User Management'] = 'Users';
$lang['Privilege Management'] = 'Privileges';
$lang['Navigation Management'] = 'Navigation';
$lang['Widget Management'] = 'Widgets';
$lang['Module Management'] = 'Modules';
$lang['Layout Management'] = 'Layouts';
$lang['Change Theme'] = 'Change Theme';
$lang['Quick Link Management'] = 'Quicklinks';
$lang['Configuration Management'] = 'Configuration';
$lang['User Info'] = 'User Info';
$lang['Share This Page !!'] = 'Share This Page !!';
$lang['Donate No-CMS'] = 'Donate No-CMS';
$lang['Welcome'] = 'Welcome';
$lang['Username already exists'] = 'Username already exists';
$lang['Username is empty'] = 'Username is empty';

//Credits
$lang['Credits'] = 'Credits';
$lang['Amount'] = 'Deposit Amount';
$lang['Credits is empty'] = 'Credits is empty';
$lang['is NOT numeric'] = 'is NOT numeric';
$lang['is less 10'] = 'is less 10';
$lang['Ok'] = 'Ok';
$lang['Success'] = 'Success';
$lang['Thank'] = 'Thank';
$lang['Paypal'] = 'Paypal';

//Orders
$lang['This is not a link YouTube'] = 'This is not a link YouTube';
$lang['For link'] = 'For link';
