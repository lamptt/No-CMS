<?php
/**
 * User: applelam
 * Date: 28/07/2015
 * Time: 17:10
 */

require 'vendor/autoload.php';


class Youtube
{
    /**
     * Helper method for getting an APIContext for all calls
     * @param string $clientId Client ID
     * @param string $clientSecret Client Secret
     * @return PayPal\Rest\ApiContext
     */
    function search($q, $maxResults)
    {
        //1. Search YouTube
        $DEVELOPER_KEY = 'AIzaSyD1OM5ag3rCYm9OPJbI5c1wo8u4cttNoHk';

        $client = new Google_Client();
        $client->setDeveloperKey($DEVELOPER_KEY);

        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);
        $objDateTime = new DateTime();
        $interval = new DateInterval('P1M');
        $objDateTime->sub($interval);
        $searchResponse = null;
        try {
            // Call the search.list method to retrieve results matching the specified
            // query term.
            $searchResponse = $youtube->search->listSearch('id,snippet', array(
                'q' => $q,
                'maxResults' => $maxResults,
                'publishedAfter' => $objDateTime->format(DateTime::RFC3339),
            ));

        } catch (Google_Service_Exception $e) {
            echo $e->getMessage();
        } catch (Google_Exception $e) {
            echo $e->getMessage();
        }
        return $searchResponse;
    }

    function getStatistics($id)
    {
        //1. Search YouTube
        $DEVELOPER_KEY = 'AIzaSyD1OM5ag3rCYm9OPJbI5c1wo8u4cttNoHk';

        $client = new Google_Client();
        $client->setDeveloperKey($DEVELOPER_KEY);

        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);
        $objDateTime = new DateTime();
        $interval = new DateInterval('P1M');
        $objDateTime->sub($interval);
        $searchResponse = null;
        try {
            // Call the search.list method to retrieve results matching the specified
            // query term.
            $searchResponse = $youtube->videos->listVideos('statistics', array(
                'id' => $id
            ));
        } catch (Google_Service_Exception $e) {
            echo $e->getMessage();
        } catch (Google_Exception $e) {
            echo $e->getMessage();
        }
        return $searchResponse;
    }
}
