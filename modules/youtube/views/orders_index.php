<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
    <style type="text/css">
        #message:empty{
            display:none;
        }
        #btn-order, .order_input, .price, .price_high{
            display:none;
        }
        .price, .price_high{
            padding-top: 7px;
            padding-left: 14px;
        }
    </style>

    <script type="text/javascript">
        var REQUEST_EXISTS = false;
        var REQUEST = "";
        function check_order(){
            var link =  $('input[name="<?=$secret_code?>link"]').val();
            var quantity =  $('input[name="<?=$secret_code?>quantity"]').val();
            var type =  $('input[name="<?=$secret_code?>type"]').attr("checked")=='checked';

            $("#img_ajax_loader").show();
            if(REQUEST_EXISTS){
                REQUEST.abort();
            }
            REQUEST_EXISTS = true;
            REQUEST = $.ajax({
                "url" : "orders/check_order",
                "type" : "POST",
                "data" : {"link":link, "quantity": quantity, "type": type},
                "dataType" : "json",
                "success" : function(data){
                    if(!data.error && !data.exists && link!='' && quantity!=''){
                        $('input[name="btn_order"]').show();
                        $('input[name="btn_order"]').removeAttr('disabled');
                        console.log($('input[name="btn_order"]'));
                    }else{
                        $('input[name="btn_order"]').hide();
                        $('input[name="btn_order"]').attr('disabled', 'disabled');
                    }

                    // get message from server + local check
                    var message = '';
                    if(data.message!=''){
                        message += data.message+'<br />';
                    }

                    if(link == ''){
                        message += '{{ language:Link is empty }}<br />';
                    }
                    if(quantity == ''){
                        message += '{{ language:Quantity is empty }}<br />';
                    }

                    if(message != $('#message').html()){
                        $('#message').html(message);
                    }
                    REQUEST_EXISTS = false;
                    $("#img_ajax_loader").hide();
                },
                error: function(xhr, textStatus, errorThrown){
                    if(textStatus != 'abort'){
                        setTimeout(check_order, 10000);
                    }
                }
            });
        }

        function toggle_type_input(){
            if($('input[name="<?=$secret_code?>type"]').prop('checked')){
                $('.price_high').show();
                $('.price').hide();
            }else{
                $('.price').show();
                $('.price_high').hide();
            }
        }

        function change_price(price, type){
            var quantity =  $('input[name="<?=$secret_code?>quantity"]').val();
            if (quantity == '') {
                toggle_type_input();
                return;
            }
            if (isNaN(quantity)) quantity = 0;
            var sum = price * 10 * parseFloat(quantity) / 10;
            if (type) {
                $('.price_high').text(sum);
                $('.price_high').show();
                $('.price').hide();
            }
            else {
                $('.price').text(sum);
                $('.price').show();
                $('.price_high').hide();
            }
            console.log(sum);
        }

        $(document).ready(function(){
            var price_high = parseFloat($('.price_high').text()), price = parseFloat($('.price').text());
            check_order();
            toggle_type_input();

            $('#form-order input').keyup(function(){
                check_order();
            });

            $('#<?=$secret_code?>quantity').keyup(function(){
                var type =  $('input[name="<?=$secret_code?>type"]').attr("checked")=='checked';
                if (type) change_price(price_high, type);
                else change_price(price, type);
            });

            $('input[name="<?=$secret_code?>type"]').change(function(){
                var type =  $('input[name="<?=$secret_code?>type"]').attr("checked")=='checked';
                if (type) change_price(price_high, type);
                else change_price(price, type);
                //toggle_type_input();
                check_order();
            });

        });
    </script>

    <h3>Đặt đơn hàng</h3>
    <h4>ValViews là một seller của xlviews nên có một số lưu ý:</h4>
    <ul>
        <li>Không nhận link rút gọn.</li>
        <li>Video không bật kiếm tiền.</li>
        <li>Video không để Private.</li>
        <li>Video để ở chế độ Track In All Country, không Block, Allow Country.</li>
        <li>Ví dụ: Link đúng:</li>
        <li>https://www.youtube.com/watch?v=12345678901</li>
    </ul>
    <h4>Chúc các bạn thành công!</h4>
<?php
echo form_open_multipart('youtube/orders', 'id="form-order" class="form form-horizontal"');
echo form_input(array('name'=>'link', 'value'=>'', 'class'=>'order_input'));
echo form_input(array('name'=>'quantity', 'value'=>'', 'class'=>'order_input'));
echo form_checkbox(array('name'=>'type', 'value'=>'', 'class'=>'order_input'));

echo '<div class="form-group">';
echo form_label('{{ language:Link (YouTube) }}', ' for="" class="control-label col-sm-4');
echo '<div class="col-sm-8">';
echo form_input($secret_code.'link', $link,
    'id="'.$secret_code.'link" placeholder="Một link YouTube" class="form-control"');
echo '</div>';
echo '</div>';

echo '<div class="form-group">';
echo form_label('{{ language:Quantity (x1000) }}', ' for="" class="control-label col-sm-4');
echo '<div class="col-sm-8">';
echo form_input($secret_code.'quantity', $quantity,
    'id="'.$secret_code.'quantity" placeholder="Phải là số tự nhiên" class="form-control"');
echo '</div>';
echo '</div>';

echo '<div class="form-group">';
echo '<div class="col-sm-offset-4 col-sm-8">';
echo form_checkbox($secret_code.'type','True',FALSE);
echo form_label('{{ language:Vip }}', ' for="" class="control-label');
echo '</div>';
echo '</div>';

echo '<div class="form-group">';
echo form_label('{{ language:Price ($) }}', ' for="" class="control-label col-sm-4');
echo '<div class="col-sm-8">';
echo '<div class="price_high">'. $price_high .'</div>';
echo '<div class="price">'. $price .'</div>';
echo '</div>';
echo '</div>';

echo '<div class="form-group"><div class="col-sm-offset-4 col-sm-8">';
echo '<img id="img_ajax_loader" style="display:none;" src="'.base_url('assets/nocms/images/ajax-loader.gif').'" /><br />';
echo '<div id="message" class="alert alert-danger"></div>';
echo form_submit('btn_order', $register_caption, 'id="btn-credits" class="btn btn-primary" ');
echo '</div></div>';
echo form_close();
?>