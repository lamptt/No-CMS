<?php
class All_orders_Model extends CMS_Model{

    /**
     * @author applelam
     * @param float credits - amount of deposit
     * @param int views - TRUE:deposit FALSE:buy view
     * @return void
     * @desc deposit
     **/
    function error($id){
        //1. get $user_credits
        $where = array(
            'views_id' => $id
        );
        $data = array(
            'views_state' => 'error',
        );
        $this->db->update('views', $data, $where);
    }

}