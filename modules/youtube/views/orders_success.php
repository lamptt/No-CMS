<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h3>{{ language:Success }} <?php echo $total.'$' ?></h3>
<p>{{ language:For link }}: <?php echo $link ?></p>
<p>{{ language:Thank }} {{ user_real_name }}!</p>