<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h3>{{ language:Success }} <?php echo $total.''.$currency; ?></h3>
<p>{{ language:Thank }} {{ user_real_name }}</p>