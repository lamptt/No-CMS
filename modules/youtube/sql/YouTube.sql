#author: applelam
#Create table views
CREATE TABLE `no_cms`.`views` (
    `views_id` INT NOT NULL AUTO_INCREMENT,
    `views_user_id` INT(20) NOT NULL,
    `views_state` ENUM('open','pending','closed') NULL DEFAULT 'open',
    `views_link` VARCHAR(60) NULL,
    `views_quantity` INT NULL,
    `views_price` FLOAT NULL,
    `views_datetime` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
    `views_frame_hours` TINYINT(2) NULL,
    PRIMARY KEY (`views_id`));
# Luu y: Khoang thoi gian cua TIMESTAMP < DATETIME --> chua ro cach thuc hoat dong cua CURRENT_TIMESTAMP

#Edit user
ALTER TABLE `no_cms`.`la_main_user`
ADD COLUMN `la_main_user_credits` FLOAT NULL DEFAULT 0 AFTER `login`,
ADD COLUMN `la_main_user_money` FLOAT NULL DEFAULT 0 AFTER `la_main_user_credits`,
ADD COLUMN `la_main_user_sumviews` INT NULL DEFAULT 0 AFTER `la_main_user_money`;

#Create paypal
CREATE TABLE `no_cms`.`paypal` (
  `paypal_id` INT NOT NULL AUTO_INCREMENT,
  `paypal_user_id` INT(20) NOT NULL,
  `paypal_payment_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`paypal_id`));

#Add type high
ALTER TABLE `no_cms`.`views`
ADD COLUMN `views_type` ENUM('normal','high') NULL DEFAULT 'normal' AFTER `views_quantity`;

#Add user vip
ALTER TABLE `no_cms`.`la_main_user`
ADD COLUMN `la_main_user_type` ENUM('normal','vip') NULL DEFAULT 'normal' AFTER `login`;

#Add report error
ALTER TABLE `no_cms`.`views`
CHANGE COLUMN `views_state` `views_state` ENUM('open','pending','closed','error') NULL DEFAULT 'open' ;

#Add poin article
ALTER TABLE `no_cms`.`la_blog_article`
ADD COLUMN `la_blog_article_poin` FLOAT NULL DEFAULT 0 AFTER `publish_date`;
