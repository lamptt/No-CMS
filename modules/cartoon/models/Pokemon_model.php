<?php
class Pokemon_Model extends CMS_Model{

    function get(){
        $query = $this->db->get('pokemons');
        // or you can use this too:
        //  $query = $this->db->query('SELECT * FROM pokemons');
        $pokemon_list = array();
        foreach($query->result() as $row){
            $pokemon_list[] = $row->name;
        }
        return $pokemon_list;
    }

}