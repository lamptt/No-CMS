<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Manage_Category
 *
 * @author No-CMS Module Generator
 */
class All_orders extends CMS_Secure_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->cms_module_path().'/all_orders_model');
    }

	public function index(){
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// initialize groceryCRUD
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $crud = $this->new_crud();
        $crud->unset_jquery();

        // set model
        // $crud->set_model($this->cms_module_path().'/grocerycrud_category_model');

        // adjust groceryCRUD's language to No-CMS's language
        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table('views');

        // set subject
        $crud->set_subject('Orders');

        // displayed columns on list
        $crud->columns('views_link','views_quantity', 'views_datetime', 'views_state');

        // required field
        $crud->required_fields('views_id');
        $crud->unique_fields('views_id');

        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_edit();
        $crud->unset_delete();

        // caption of each columns
        $crud->display_as('views_link','Link');
        $crud->display_as('views_quantity','Quantity');
        $crud->display_as('views_datetime','DateTime');
        $crud->display_as('views_state','State');

        $crud->add_action('Báo cáo lỗi', '', '','btn btn-default', array($this,'just_a_test'));

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// HINT: Put set relation (lookup) codes here
		// (documentation: http://www.grocerycrud.com/documentation/options_functions/set_relation)
		// eg:
		// 		$crud->set_relation( $field_name , $related_table, $related_title_field , $where , $order_by );
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// HINT: Put set relation_n_n (detail many to many) codes here
		// (documentation: http://www.grocerycrud.com/documentation/options_functions/set_relation_n_n)
		// eg:
		// 		$crud->set_relation_n_n( $field_name, $relation_table, $selection_table, $primary_key_alias_to_this_table,
		// 			$primary_key_alias_to_selection_table , $title_field_selection_table, $priority_field_relation );
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/*$crud->set_relation_n_n('articles',
		    $this->cms_complete_table_name('category_article'),
		    $this->cms_complete_table_name('article'),
			'category_id', 'article_id',
			'article_title', NULL);*/

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// HINT: Put custom field type here
		// (documentation: http://www.grocerycrud.com/documentation/options_functions/field_type)
		// eg:
		// 		$crud->field_type( $field_name , $field_type, $value  );
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //$crud->field_type('views_state', 'enum', array('open','pending','closed'));
        //$crud->unset_texteditor('description');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// HINT: Put callback here
		// (documentation: httm://www.grocerycrud.com/documentation/options_functions)
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/*$crud->callback_before_insert(array($this,'before_insert'));
		$crud->callback_before_update(array($this,'before_update'));
		$crud->callback_before_delete(array($this,'before_delete'));
		$crud->callback_after_insert(array($this,'after_insert'));
		$crud->callback_after_update(array($this,'after_update'));
		$crud->callback_after_delete(array($this,'after_delete'));*/

        $crud->where('views_user_id', $this->cms_user_id())
            ->order_by('views_id', 'DESC');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();

        // prepare css & js, add them to config
        $config = array();
        $asset = new Cms_asset();
        foreach($output->css_files as $file){
            $asset->add_css($file);
        }
        $config['css'] = $asset->compile_css();

        foreach($output->js_files as $file){
            $asset->add_js($file);
        }
        $config['js'] = $asset->compile_js();

        // show the view
        $this->view('youtube/all_orders_view', $output,
            'youtube_all_orders', $config);

    }
    function just_a_test($primary_key , $row)
    {
        if ($row->views_state == 'closed') return site_url('youtube/all_orders/error').'/'.$row->views_id;
        else return site_url('youtube/all_orders/error/-1');
    }
    public function error($id){
        $this->all_orders_model->error($id);
        redirect('/youtube/all_orders');
    }
}