<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Info extends CMS_Module {

    /////////////////////////////////////////////////////////////////////////////
    // Default Functions
    /////////////////////////////////////////////////////////////////////////////

    // ACTIVATION
    public function do_activate(){
        $this->remove_all();
        $this->build_all();
    }

    // DEACTIVATION
    public function do_deactivate(){
        $this->backup_database(array(
            'pokemons',
        ));
        $this->remove_all();
    }

    // UPGRADE
    public function do_upgrade($old_version){
        // Add your migration logic here.
    }

    /////////////////////////////////////////////////////////////////////////////
    // Private Functions
    /////////////////////////////////////////////////////////////////////////////

    // REMOVE ALL NAVIGATIONS, WIDGETS, AND PRIVILEGES
    private function remove_all(){

        // remove navigations
        $this->cms_remove_navigation('pokemon_list');

        // drop table
        $this->dbforge->drop_table('pokemons', TRUE);
        // If you prefer to work with raw SQL, this one will also works:
        // $this->db->query('DROP TABLE IF EXISTS `pokemons`');
    }

    // CREATE ALL NAVIGATIONS, WIDGETS, AND PRIVILEGES
    private function build_all(){
        $module_path = $this->cms_module_path();

        // parent of all navigations
        $this->cms_add_navigation('pokemon_list', 'Pokemon List',
            $module_path.'/pokemon/show', $this->PRIV_EVERYONE);

        // create table
        $fields = array(
            'id'=> $this->TYPE_INT_UNSIGNED_AUTO_INCREMENT,
            'name'=> array("type"=>'varchar', "constraint"=>45, "null"=>TRUE),
            'description' => array("type"=>'text'),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('pokemons');
        // If you prefer to work with raw SQL, this one will also works:
        // $this->db->query('CREATE TABLE `pokemons` (
        //      `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
        //      `name` varchar(45) DEFAULT NULL,
        //      `description` text,
        //      PRIMARY KEY (`id`)
        //    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
    }

    // EXPORT DATABASE
    private function backup_database($table_names, $limit = 100){
        $module_path = $this->cms_module_path();

        $this->load->dbutil();
        $sql = '';

        // create DROP TABLE syntax
        for($i=count($table_names)-1; $i>=0; $i--){
            $table_name = $table_names[$i];
            $sql .= 'DROP TABLE IF EXISTS `'.$table_name.'`; '.PHP_EOL;
        }
        if($sql !='')$sql.= PHP_EOL;

        // create CREATE TABLE and INSERT syntax
        $prefs = array(
            'tables'      => $table_names,
            'ignore'      => array(),
            'format'      => 'txt',
            'filename'    => 'mybackup.sql',
            'add_drop'    => FALSE,
            'add_insert'  => TRUE,
            'newline'     => PHP_EOL
        );
        $sql.= $this->dbutil->backup($prefs);

        //write file
        $file_name = 'backup_'.date('Y-m-d_G-i-s').'.sql';
        file_put_contents(
            BASEPATH.'../modules/'.$module_path.'/assets/db/'.$file_name,
            $sql
        );

    }
}