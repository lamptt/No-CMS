<?php
/**
 * User: applelam
 * Date: 28/07/2015
 * Time: 17:10
 */

require 'vendor/autoload.php';

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

// bootstrap
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

// ExecutePayment
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;

class Paypal
{
    /**
     * Helper method for getting an APIContext for all calls
     * @param string $clientId Client ID
     * @param string $clientSecret Client Secret
     * @return PayPal\Rest\ApiContext
     */
    function getApiContext()
    {
        // Replace these values by entering your own ClientId and Secret by visiting https://developer.paypal.com/webapps/developer/applications/myapps
        $clientId = 'Aa5VijPnXA5WbCMu4hjU79g6rVt037oLC8NhLQpzVItvEFdZw0kSFdoUufecmU3fyGXVsM0pDngSl0Ys';
        $clientSecret = 'EHfH_HWhKtRj0DKonal2_2hSLZ2Nqh-U1z4AwGYB6QnvycR94SlZaBs1dBGoMojL6jaFpMzmuuS7xqcG';

        // #### SDK configuration
        // Register the sdk_config.ini file in current directory
        // as the configuration source.
        /*
        if(!defined("PP_CONFIG_PATH")) {
            define("PP_CONFIG_PATH", __DIR__);
        }
        */


        // ### Api context
        // Use an ApiContext object to authenticate
        // API calls. The clientId and clientSecret for the
        // OAuthTokenCredential class can be retrieved from
        // developer.paypal.com

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );

        // Comment this line out and uncomment the PP_CONFIG_PATH
        // 'define' block if you want to use static file
        // based configuration

        $apiContext->setConfig(
            array(
                'mode' => 'sandbox',
                'log.LogEnabled' => true,
                'log.FileName' => '../PayPal.log',
                'log.LogLevel' => 'DEBUG', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'validation.level' => 'log',
                'cache.enabled' => true,
                // 'http.CURLOPT_CONNECTTIMEOUT' => 30
                // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
            )
        );

        // Partner Attribution Id
        // Use this header if you are a PayPal partner. Specify a unique BN Code to receive revenue attribution.
        // To learn more or to request a BN Code, contact your Partner Manager or visit the PayPal Partner Portal
        // $apiContext->addRequestHeader('PayPal-Partner-Attribution-Id', '123123123');

        return $apiContext;
    }

    function payments($credits)
    {
        /** @var \Paypal\Rest\ApiContext $apiContext */
        $apiContext = $this->getApiContext();

        // ### Payer
        // A resource representing a Payer that funds a payment
        // For paypal account payments, set payment method
        // to 'paypal'.
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // ### Itemized information
        // (Optional) Lets you specify item wise
        // information
        $item1 = new Item();
        $item1->setName('Deposit Amount')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku("68")// Similar to `item_number` in Classic API
            ->setPrice($credits);

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        // ### Additional payment details
        // Use this optional field to set additional
        // payment information such as tax, shipping
        // charges etc.
        $details = new Details();
        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($credits);

        // ### Amount
        // Lets you specify a payment amount.
        // You can also specify additional details
        // such as shipping, tax.
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($credits)
            ->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it.
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after
        // payment approval/ cancellation.
        $baseUrl = getBaseUrl();
        $redirectUrls = new RedirectUrls();

        //Set with apache
        $redirectUrls->setReturnUrl("$baseUrl/credits/executepayment?success=true")
            ->setCancelUrl("$baseUrl/credits/executepayment?success=false");

        //Set with nginx
        /*$redirectUrls->setReturnUrl("$baseUrl/youtube/credits/executepayment?success=true")
            ->setCancelUrl("$baseUrl/youtube/credits/executepayment?success=false");*/

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));


        // For Sample Purposes Only.
        $request = clone $payment;

        // ### Create Payment
        // Create a payment by calling the 'create' method
        // passing it a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the state and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {
            $payment->create($apiContext);
        } catch (Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            // ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
            exit(1);
        }

        // ### Get redirect url
        // The API response provides the url that you must redirect
        // the buyer to. Retrieve the url from the $payment->getApprovalLink()
        // method
        $approvalUrl = $payment->getApprovalLink();

        // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
        //ResultPrinter::printResult("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", "<a href='$approvalUrl' >$approvalUrl</a>", $request, $payment);

        //return $payment;
        return $approvalUrl;
    }

    function executepayment()
    {
        // ### Approval Status
        // Determine if the user approved the payment or not
        if (isset($_GET['success']) && $_GET['success'] == 'true') {
            /** @var \Paypal\Rest\ApiContext $apiContext */
            $apiContext = $this->getApiContext();
            // Get the payment Object by passing paymentId
            // payment id was previously stored in session in
            // CreatePaymentUsingPayPal.php
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $apiContext);

            // ### Payment Execute
            // PaymentExecution object includes information necessary
            // to execute a PayPal account payment.
            // The payer_id is added to the request query parameters
            // when the user is redirected from paypal back to your site
            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);

            try {
                // Execute the payment
                // (See bootstrap.php for more on `ApiContext`)
                $result = $payment->execute($execution, $apiContext);

                // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                // ResultPrinter::printResult("Executed Payment", "Payment", $payment->getId(), $execution, $result);

                try {
                    $payment = Payment::get($paymentId, $apiContext);
                } catch (Exception $ex) {
                    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                    //ResultPrinter::printError("Get Payment", "Payment", null, null, $ex);
                    redirect('','refresh');
                    exit(1);
                }
            } catch (Exception $ex) {
                // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                //ResultPrinter::printError("Executed Payment", "Payment", null, null, $ex);
                redirect('','refresh');
                exit(1);
            }

            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            // ResultPrinter::printResult("Get Payment", "Payment", $payment->getId(), null, $payment);

            return $payment;


        } else {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            //ResultPrinter::printResult("User Cancelled the Approval", null);
            exit;
        }
    }
}


/**
 * Helper Class for Printing Results
 *
 * Class ResultPrinter
 */
class ResultPrinter
{

    private static $printResultCounter = 0;

    /**
     * Prints HTML Output to web page.
     *
     * @param string     $title
     * @param string    $objectName
     * @param string    $objectId
     * @param mixed     $request
     * @param mixed     $response
     * @param string $errorMessage
     */
    public static function printOutput($title, $objectName, $objectId = null, $request = null, $response = null, $errorMessage = null)
    {
        if (PHP_SAPI == 'cli') {
            self::$printResultCounter++;
            printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
            printf("(%d) %s", self::$printResultCounter, strtoupper($title));
            printf("\n-------------------------------------------------------------\n\n");
            if ($objectId) {
                printf("Object with ID: %s \n", $objectId);
            }
            printf("-------------------------------------------------------------\n");
            printf("\tREQUEST:\n");
            self::printConsoleObject($request);
            printf("\n\n\tRESPONSE:\n");
            self::printConsoleObject($response, $errorMessage);
            printf("\n-------------------------------------------------------------\n\n");
        } else {

            if (self::$printResultCounter == 0) {
                include "header.html";
                echo '
                  <div class="row header"><div class="col-md-5 pull-left"><br /><a href="../index.php"><h1 class="home">&#10094;&#10094; Back to Samples</h1></a><br /></div> <br />
                  <div class="col-md-4 pull-right"><img src="https://www.paypalobjects.com/webstatic/developer/logo2_paypal_developer_2x.png" class="logo" width="300"/></div> </div>';
                echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
            }
            self::$printResultCounter++;
            echo '
        <div class="panel panel-default">
            <div class="panel-heading '. ($errorMessage ? 'error' : '') .'" role="tab" id="heading-'.self::$printResultCounter.'">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#step-'. self::$printResultCounter .'" aria-expanded="false" aria-controls="step-'.self::$printResultCounter.'">
            '. self::$printResultCounter .'. '. $title . ($errorMessage ? ' (Failed)' : '') . '</a>
                </h4>
            </div>
            <div id="step-'.self::$printResultCounter.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-'. self::$printResultCounter . '">
                <div class="panel-body">
            ';

            if ($objectId) {
                echo "<div>" . ($objectName ? $objectName : "Object") . " with ID: $objectId </div>";
            }

            echo '<div class="row hidden-xs hidden-sm hidden-md"><div class="col-md-6"><h4>Request Object</h4>';
            self::printObject($request);
            echo '</div><div class="col-md-6"><h4 class="'. ($errorMessage ? 'error' : '') .'">Response Object</h4>';
            self::printObject($response, $errorMessage);
            echo '</div></div>';

            echo '<div class="hidden-lg"><ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" ><a href="#step-'.self::$printResultCounter .'-request" role="tab" data-toggle="tab">Request</a></li>
                        <li role="presentation" class="active"><a href="#step-'.self::$printResultCounter .'-response" role="tab" data-toggle="tab">Response</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane" id="step-'.self::$printResultCounter .'-request"><h4>Request Object</h4>';
            self::printObject($request) ;
            echo '</div><div role="tabpanel" class="tab-pane active" id="step-'.self::$printResultCounter .'-response"><h4>Response Object</h4>';
            self::printObject($response, $errorMessage);
            echo '</div></div></div></div>
            </div>
        </div>';
        }
        flush();
    }

    /**
     * Prints success response HTML Output to web page.
     *
     * @param string     $title
     * @param string    $objectName
     * @param string    $objectId
     * @param mixed     $request
     * @param mixed     $response
     */
    public static function printResult($title, $objectName, $objectId = null, $request = null, $response = null)
    {
        self::printOutput($title, $objectName, $objectId, $request, $response, false);
    }

    /**
     * Prints Error
     *
     * @param      $title
     * @param      $objectName
     * @param null $objectId
     * @param null $request
     * @param \Exception $exception
     */
    public static function printError($title, $objectName, $objectId = null, $request = null, $exception = null)
    {
        $data = null;
        if ($exception instanceof \PayPal\Exception\PayPalConnectionException) {
            $data = $exception->getData();
        }
        self::printOutput($title, $objectName, $objectId, $request, $data, $exception->getMessage());
    }

    protected static function printConsoleObject($object, $error = null)
    {
        if ($error) {
            echo 'ERROR:'. $error;
        }
        if ($object) {
            if (is_a($object, 'PayPal\Common\PayPalModel')) {
                /** @var $object \PayPal\Common\PayPalModel */
                echo $object->toJSON(128);
            } elseif (is_string($object) && \PayPal\Validation\JsonValidator::validate($object, true)) {
                echo str_replace('\\/', '/', json_encode(json_decode($object), 128));
            } elseif (is_string($object)) {
                echo $object;
            } else {
                print_r($object);
            }
        } else {
            echo "No Data";
        }
    }

    protected static function printObject($object, $error = null)
    {
        if ($error) {
            echo '<p class="error"><i class="fa fa-exclamation-triangle"></i> '.
                $error.
                '</p>';
        }
        if ($object) {
            if (is_a($object, 'PayPal\Common\PayPalModel')) {
                /** @var $object \PayPal\Common\PayPalModel */
                echo '<pre class="prettyprint '. ($error ? 'error' : '') .'">' . $object->toJSON(128) . "</pre>";
            } elseif (is_string($object) && \PayPal\Validation\JsonValidator::validate($object, true)) {
                echo '<pre class="prettyprint '. ($error ? 'error' : '') .'">'. str_replace('\\/', '/', json_encode(json_decode($object), 128)) . "</pre>";
            } elseif (is_string($object)) {
                echo '<pre class="prettyprint '. ($error ? 'error' : '') .'">' . $object . '</pre>';
            } else {
                echo "<pre>";
                print_r($object);
                echo "</pre>";
            }
        } else {
            echo "<span>No Data</span>";
        }
    }
}

/**
 * ### getBaseUrl function
 * // utility function that returns base url for
 * // determining return/cancel urls
 *
 * @return string
 */
function getBaseUrl()
{
    if (PHP_SAPI == 'cli') {
        $trace=debug_backtrace();
        $relativePath = substr(dirname($trace[0]['file']), strlen(dirname(dirname(__FILE__))));
        echo "Warning: This sample may require a server to handle return URL. Cannot execute in command line. Defaulting URL to http://localhost$relativePath \n";
        return "http://localhost" . $relativePath;
    }
    $protocol = 'http';
    if ($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')) {
        $protocol .= 's';
    }
    $host = $_SERVER['HTTP_HOST'];
    $request = $_SERVER['PHP_SELF'];
    return dirname($protocol . '://' . $host . $request);
}