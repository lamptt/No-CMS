<?php
$lang['language_alias'] = 'vi';
$lang['Complete Menu'] = 'Quản trị';
$lang['Home'] = 'Trang chủ';
$lang['Language'] = 'Ngôn ngữ';
$lang['Third Party Authentication'] = 'Mạng xã hội';
$lang['Login'] = 'Đăng nhập';
$lang['Logout'] = 'Đăng xuất';
$lang['Forgot Password'] = 'Quên mật khẩu';
$lang['Register'] = 'Đăng ký';
$lang['No-CMS User Guide'] = 'Hướng dẫn';
$lang['Change Profile'] = 'Hồ sơ cá nhân';
$lang['CMS Management'] = 'Quản trị Admin';
$lang['Group Management'] = 'Nhóm';
$lang['User Management'] = 'Người dùng';
$lang['Privilege Management'] = 'Privileges';
$lang['Navigation Management'] = 'Navigation';
$lang['Widget Management'] = 'Widgets';
$lang['Module Management'] = 'Modules';
$lang['Layout Management'] = 'Layouts';
$lang['Change Theme'] = 'Change Theme';
$lang['Quick Link Management'] = 'Quicklinks';
$lang['Configuration Management'] = 'Configuration';
$lang['User Info'] = 'Thông tin người dùng';
$lang['Share This Page !!'] = 'Chia sẻ trang';
$lang['Donate No-CMS'] = 'Ủng hộ chúng tôi';
$lang['Welcome'] = 'Xin chào';
$lang['Username already exists'] = 'Username đã tồn tại';
$lang['Username is empty'] = 'Username rỗng';

//Credits
$lang['Credits'] = 'Tín dụng';
$lang['Amount'] = 'Số lượng gửi';
$lang['Credits is empty'] = 'Không được rỗng';
$lang['is NOT numeric'] = 'Phải là số';
$lang['is less 10'] = 'Không được nhỏ hơn 10';
$lang['Ok'] = 'Đồng ý';
$lang['Success'] = 'Thành công';
$lang['Thank'] = 'Cám ơn';
$lang['Paypal'] = 'Chọn phương thức thanh toán';
$lang['paypal-title'] = 'Đối với Paypal và Visa:';
$lang['new-payment-title'] = 'Đối với ngân hàng Việt Nam:';
$lang['new-payment-link'] = 'Các bước nạp tiền đối với ngân hàng Việt Nam';

//Orders
$lang['This is not a link YouTube'] = 'This is not a link YouTube';
$lang['For link'] = 'For link';
$lang['Vip'] = 'Lượng xem video 90%';
$lang['error-hour'] = 'Chưa được thực hiện khung giờ này';

//login
$lang['Identity'] = 'Tên đăng nhập';
$lang['Password'] = 'Mật khẩu';

//register
$lang['User Name'] = 'Tên đăng nhập';
$lang['Email'] = 'Mail';
$lang['Real Name'] = 'Tên thật';
$lang['Confirm Password'] = 'Gõ lại mật khẩu';