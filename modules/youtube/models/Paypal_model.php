<?php
class Paypal_Model extends CMS_Model{

    /**
     * @author applelam
     * @param
     * @return void
     * @desc deposit
     **/
    function create($payment_id){
        $data = array(
            'paypal_user_id'=> $this->cms_user_id(),
            'paypal_payment_id' => $payment_id,
        );
        $this->db->insert('paypal', $data);
    }

}