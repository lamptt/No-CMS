<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php echo $user_real_name.' '.$user_credits.'$'; ?><br />
    <div class="progress">
        <div class="progress-bar progress-bar-striped active" role="progressbar"
             aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{ user_exp }}%">
            EXP
        </div>
    </div>
<?php echo anchor('main/logout', $logout_lang);