<?php
class Pokemon extends CMS_Secure_Controller{

    function showtest(){
        echo '<ul>
                    <li>pikachu</li>
                    <li>charmender</li>
                    <li>bulbasur</li>
                    <li>squirtle</li>
                </ul>';
    }

    function showtest2(){
        $pokemon_list = array('pikachu',
            'bulbasur', 'charmender',
            'squirtle', 'caterpie',
            'articuno', 'ekans', 'koffing'
        );
        $data['pokemon_list'] = $pokemon_list;
        $data['name'] = 'lamptt';
        $this->view($this->cms_module_path().'/pokemon_index', $data);
    }

    function show(){
        $this->load->model($this->cms_module_path().'/pokemon_model');
        $data['pokemon_list'] = $this->pokemon_model->get();
        $data['name'] = 'Lamptt Model';
        $this->view($this->cms_module_path().'/pokemon_index', $data);
    }
}