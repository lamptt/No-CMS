<?php

class Search extends CMS_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('youtube/youtube');
    }

    private function __random_string($length = 10)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $size = strlen($chars);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str .= '_';
    }

    /**
     * @author applelam
     * @return void
     * @desc orders
     **/
    function index()
    {
        // the honey_pot, every fake input should be empty
        $honey_pot_pass = (strlen($this->input->post('title', '')) == 0
            && (strlen($this->input->post('quantity', '')) == 0)
        );
        if (!$honey_pot_pass) {
            show_404();
            die();
        }

        $previous_secret_code = $this->session->userdata('__search_code');
        if ($previous_secret_code === NULL) {
            $previous_secret_code = $this->__random_string();
        }

        //get input
        $link = $this->input->post($previous_secret_code . 'title');
        $quantity = intval($this->input->post($previous_secret_code . 'quantity'));

        //set validation rule
        $this->form_validation->set_rules($previous_secret_code . 'title', 'Title', 'required');
        $this->form_validation->set_rules($previous_secret_code . 'quantity', 'Quantity', 'required|is_natural_no_zero');

        // generate new secret code
        $secret_code = $this->__random_string();
        $this->session->set_userdata('__search_code', $secret_code);
        $searchResponse = null;
        if ($this->form_validation->run()) {
            $searchResponse = $this->youtube->search($link, $quantity);
        }
        //i_kF4zLNKio
        //$a = $this->youtube->getStatistics('i_kF4zLNKio');
        //$this->youtube->getStatistics('i_kF4zLNKio')['items'][0]['statistics']['viewCount'];
        if ($searchResponse != null){
            $videos = '';
            $channels = '';
            $playlists = '';
            foreach ($searchResponse['items'] as $searchResult) {
                switch ($searchResult['id']['kind']) {
                    case 'youtube#video':
                        $videos .= sprintf('<li>Ngày xuất bản: %s, Video: <a href="https://www.youtube.com/watch?v=%s">%s</a>, Lượt xem: %s</li>',
                            date('d-m-Y', strtotime($searchResult['snippet']['publishedAt'])), $searchResult['id']['videoId'], $searchResult['snippet']['title'], $this->youtube->getStatistics($searchResult['id']['videoId'])['items'][0]['statistics']['viewCount']);
                        break;
                    case 'youtube#channel':
                        $channels .= sprintf('<li>%s (%s)</li>',
                            $searchResult['snippet']['title'], $searchResult['id']['channelId']);
                        break;
                    case 'youtube#playlist':
                        $playlists .= sprintf('<li>%s (%s)</li>',
                            $searchResult['snippet']['title'], $searchResult['id']['playlistId']);
                        break;
                }
            }
            $htmlBody = '';
            $htmlBody .= <<<END
            <h3>Videos</h3>
            <ul>$videos</ul>
            <h3>Channels</h3>
            <ul>$channels</ul>
            <h3>Playlists</h3>
            <ul>$playlists</ul>
END;
            $data = array(
                "htmlBody" => $htmlBody,
                "link" => $link,
                "quantity" => $quantity,
                "secret_code" => $secret_code,
            );
            $this->view($this->cms_module_path() . '/search_index', $data);
        }
        else{
            $data = array(
                "htmlBody" => '',
                "link" => $link,
                "quantity" => $quantity,
                "secret_code" => $secret_code,
            );
            $this->view($this->cms_module_path() . '/search_index', $data);
        }
    }
}